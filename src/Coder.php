<?php

use config\CoderConfigInterface;

class Coder
{
    private array $hash = [];
    private array $reverse_hash = [];

    public function __construct(private readonly CoderConfigInterface $config)
    {
        $index = 97;
        foreach (mb_str_split($config->char_list, encoding: $this->config->coding_type) as $char){
            $this->hash[$this->convertChar($char)] = $index;
            $index++;
        }
        $this->reverse_hash = array_flip($this->hash);
    }

    /** Decodes string by given hash
     * @param string $text_to_decode
     * @return string - decoded string
     */
    public function decode(string $text_to_decode) : string
    {
        $decoded_text = "";
        foreach (mb_str_split($text_to_decode, encoding: $this->config->coding_type) as $char){
            $decoded_text .= $this->getChar($char);
        }
        return $decoded_text;
    }

    public function encode(string $text_to_encode) : string
    {
        $encoded_text = "";
        foreach (mb_str_split($text_to_encode, encoding: $this->config->coding_type) as $char){
            $encoded_text .= $this->reverse_hash[mb_ord($char, $this->config->coding_type)] ?? $char;
        }
        return $encoded_text;

    }

    /** Returns single char by key
     * @param string $char
     * @return string
     */
    protected function getChar(string $char): string
    {
        $converted_char = $this->convertChar($char);
        //if we found a char in coded list we encode it by given encoding
        if(key_exists($converted_char, $this->hash)){
            return $this->decodeChar($this->hash[$converted_char]);
        }
        //else we return base val
        return $char;
    }

    /** [hook] Converts char to given from config encoding
     * @param string $char
     * @return bool|string
     */
    protected function convertChar(string $char): bool|string
    {
        return mb_convert_encoding($char, $this->config->coding_type);
    }

    /** [hook] Encodes char from given from config encoding to text
     * @param string $encoded_char
     * @return string
     */
    protected function decodeChar(string $encoded_char): string
    {
        return mb_chr($encoded_char, $this->config->coding_type);
    }
}