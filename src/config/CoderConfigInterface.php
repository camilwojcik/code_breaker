<?php

namespace config;
abstract class CoderConfigInterface
{
    public string $coding_type = "UTF-8";
    public string $char_list = "!)\"(£*%&><@abcdefghijklmno";
}