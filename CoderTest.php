<?php

include "src/config/CoderConfigInterface.php";
include "src/config/BaseConfig.php";
include "src/Coder.php";

use config\BaseConfig;
use PHPUnit\Framework\TestCase;

class CoderTest extends TestCase
{
    public function testDecode()
    {
        $config = new BaseConfig();
        $coder = new Coder($config);
        $test = ")g!ld, j(!ad \"> h>£ gdol>!o!\" o!(!c>£";
        $decoded = $coder->decode($test);
        self::assertEquals("brawo, udalo ci sie rozwiazac zadanie", $decoded);
    }

    public function testEncode()
    {
        $config = new BaseConfig();
        $coder = new Coder($config);

        $test = "Zażółć, gęślą jaźń.";
        $encoded = $coder->encode($test);
        $decoded = $coder->decode($encoded);
        self::assertEquals($test, $decoded);
    }
}
