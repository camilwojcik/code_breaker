<?php

use config\BaseConfig;

require 'vendor/autoload.php';
include "src/config/CoderConfigInterface.php";
include "src/config/BaseConfig.php";
include "src/Coder.php";


$config = new BaseConfig();
$coder = new Coder($config);
$test = "l";
echo "test -> " . $test;
echo "<br>";

$encoded = $coder->encode($test);
echo "encoded -> " . $encoded;
echo "<br>";

$decoded = $coder->decode($encoded);
echo "decoded -> " . $decoded;
echo "<br>";

echo "Odszyfrowana wiadomość -> " . $coder->decode(')g!ld, j(!ad "> h>£ gdol>!o!" o!(!c>£');