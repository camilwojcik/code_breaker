# code_breaker

## 1.) Zadanie wymagane - Łamacz kodu
Mając klucz rozszyfrowujący, stwórz program który odkoduje poniższą, zaszyfrowaną
wiadomość.

Każdemu znakowi z klucza przyporządkowany jest znak z alfabetu za pomocą którego
powinien zostać odszyfrowany. 

Np.: za pomocą znaku @ została zaszyfrowana litera k.
Jeżeli danego znaku nie ma w kluczu to powinien on znaleźć się w odszyfrowanej wiadomości w niezmienionej formie.

## Klucz z przypisanym alfabetem:
! ) " ( £ * % & > < @ a b c d e f g h i j k l m n o

a b c d e f g h i j k l m n o p q r s t u v w x y z

## Zaszyfrowana wiadomość:
g!ld, j(!ad "> h>£ gdol>!o!" o!(!c>£

## Zadanie dodatkowe. 
Rozbuduj program o możliwość szyfrowania. Zaszyfruj poniższą
wiadomość a później przepuść ją przez deszyfrator i sprawdź czy wygląda ona tak samo.

Zażółć, gęślą jaźń

## Dodatkowo dodany plik index.php
Aby ułatwić sprawdzenie bez pobierania paczki composerem :)

Postaw serwer: php -S localhost:8080

I sprawdz rezultat!